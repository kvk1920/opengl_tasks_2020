#pragma once

#include "OpenGL.hpp"

class Interactive
{
public:
    virtual ~Interactive() = default;

    virtual void update(GLFWwindow* window, double delta_time) = 0;
    virtual void handleKey(GLFWwindow* window, int key, int action, int mods) = 0;
    virtual void handleMouseMove(GLFWwindow* window, double xpos, double ypos) = 0;
    virtual void handleScroll(GLFWwindow* window, double xoffset, double yoffset) = 0;
};
