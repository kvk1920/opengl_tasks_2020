#pragma once

#include "OpenGL.hpp"

#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>

class Shader
{
    friend class ShaderLoader;
    friend class ShaderProgram;

    GLuint shader_id;

protected:
    Shader(std::string name, GLenum shader_type);

public:
    virtual ~Shader();
};

using ShaderPtr = std::shared_ptr<Shader>;

class ShaderLoader
{
    std::unordered_map<std::string, ShaderPtr>
    vertex_shaders,
    fragment_shaders;

public:
    ShaderPtr loadVertexShader(const std::string& name);
    ShaderPtr loadFragmentShader(const std::string& name);
};

class ShaderProgram
{
    GLuint program_id;
    ShaderPtr vertex_shader;
    ShaderPtr fragment_shader;
    std::map<std::string, std::pair<int, glm::mat4>> mat4_uniforms;
    std::map<std::string, std::pair<int, glm::vec4>> vec4_uniforms;

    ShaderProgram(ShaderPtr vertex_shader, ShaderPtr fragment_shader);

public:
    ~ShaderProgram();

    static std::shared_ptr<ShaderProgram> make(ShaderLoader& loader,
                                               const std::string& vertex_shader_name,
                                               const std::string& fragment_shader_name);

    static std::shared_ptr<ShaderProgram> make(ShaderPtr vertex_shader,
                                               ShaderPtr fragment_shader);

    void use() const;
    void requireUniformMat4(const std::string& name);
    void requireUniformVec4(const std::string& name);
    void setUniformMat4(const std::string& name, glm::mat4 value);
    void setUniformVec4(const std::string& name, glm::vec4 value);
};

using ShaderProgramPtr = std::shared_ptr<ShaderProgram>;

class ShaderProgramPool
{
    std::unordered_map<std::string, ShaderProgramPtr> pool;

public:
    ShaderProgramPtr createProgram(
        std::string program_name,
        ShaderPtr vertex,
        ShaderPtr fragment);
    ShaderProgramPtr createProgram(
        std::string program_name,
        ShaderLoader& loader,
        std::string vertex,
        std::string fragment);
    ShaderProgramPtr getProgram(const std::string& program_name) const;
};
