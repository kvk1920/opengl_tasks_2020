#pragma once

#include <filesystem>
#include <string_view>

std::filesystem::path requireResourcePath(std::string_view resource_name);
