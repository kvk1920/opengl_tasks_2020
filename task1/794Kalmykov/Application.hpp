#pragma once

#include "Camera.hpp"
#include "Drawable.hpp"
#include "Interactive.hpp"
#include "Map.hpp"
#include "Model.hpp"
#include "OpenGL.hpp"
#include "Shader.hpp"

#include <vector>

class Application
    : protected Interactive
{
    ShaderLoader shader_loader{};
    ShaderProgramPool program_pool{};
    GLFWwindow* window;
    double old_time = 0;
    CameraPtr god_camera;
    CameraPtr player_camera;
    CameraPtr active_camera;
    ModelPtr wall_model;
    Map map;

    void initContext();
    void initGL();
    void initCamera();
    void initShaders();
    void initModels();
    void makeScene();

    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouseCursorPosCallback(GLFWwindow* window, double xpos, double ypos);
    static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

    void update();
    void draw();
    void run();

    void update(GLFWwindow* window, double delta_time) override;
    void handleKey(GLFWwindow* window, int key, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
public:
    Application() = default;
    ~Application() override;
    void start();

    static Application* getApplication(GLFWwindow* window);
};
