#pragma once

#include "Drawable.hpp"

class Map
{
    std::vector<DrawablePtr> objects;
    ModelPtr wall_model;

public:
    void setModel(ModelPtr wall_model1);
    void load(std::string str);
    void draw(const Camera& camera);
};
