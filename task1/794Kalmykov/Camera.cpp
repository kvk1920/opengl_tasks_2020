#include "Camera.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/geometric.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <iostream>

glm::mat4 Camera::createProjectionMatrix(GLFWwindow *window)
{
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    return glm::perspective(glm::radians(45.),
                            static_cast<double>(width) / height,
                            near_plane,
                            far_plane);
}

Camera::Camera(glm::mat4 view_matrix, GLFWwindow* window)
    : projection_matrix(createProjectionMatrix(window))
    , view_matrix(view_matrix)
{
    updateFinalMatrix();
}

void Camera::updateFinalMatrix()
{
    vp_matrix = projection_matrix * view_matrix;
}

void Camera::update(GLFWwindow* window, double delta_time)
{
    projection_matrix = createProjectionMatrix(window);
    updateFinalMatrix();
}

struct PlayerCamera
    : public Camera
{
    double old_mouse_x_pos = 0.;
    double old_mouse_y_pos = 0.;

    glm::vec2 position{};
    glm::vec2 direction{};
    double vertical_angle{};

    static glm::mat4 createViewMatrix(glm::vec2 position, glm::vec2 direction, double vertical_angle)
    {
        glm::vec3 pos3(position, .5f);
        glm::vec3 dir3(direction * (float) glm::cos(vertical_angle), glm::sin(vertical_angle));
        return glm::lookAt(pos3, pos3 + dir3, {0, 0, 1});
    }

    void update(GLFWwindow* window, double delta_time) override
    {
        Camera::update(window, delta_time);

        double speed = 1.;
        auto step = float(speed * delta_time);

        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W)) {
            position += direction * step;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S)) {
            position -= direction * step;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D)) {
            position += glm::vec2(direction.y, -direction.x) * step;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A)) {
            position -= glm::vec2(direction.y, -direction.x) * step;
        }
        view_matrix = createViewMatrix(position, direction, vertical_angle);
        updateFinalMatrix();
    }

    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override {}

    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override
    {
        double dx = xpos - old_mouse_x_pos;
        double dy = ypos - old_mouse_y_pos;

        if (GLFW_PRESS == glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
            auto v_angle = -float(.001 * dy);
            auto h_angle = -float(.001 * dx);

            vertical_angle += v_angle;
            vertical_angle = glm::clamp(vertical_angle, -glm::pi<double>() * .49, glm::pi<double>() * .49);
            direction = {glm::cos(h_angle) * direction.x - glm::sin(h_angle) * direction.y,
                         glm::sin(h_angle) * direction.x + glm::cos(h_angle) * direction.y};
        }

        view_matrix = createViewMatrix(position, direction, vertical_angle);
        updateFinalMatrix();

        old_mouse_x_pos = xpos;
        old_mouse_y_pos = ypos;
    }

    void handleKey(GLFWwindow* window, int key, int action, int mods) override
    { }

    explicit PlayerCamera(GLFWwindow* window)
        : Camera(createViewMatrix({0, 0}, {0, 1}, 0), window)
    {
        position = {0, 0};
        direction = {0, 1};
        vertical_angle = 0;
        glfwGetCursorPos(window, &old_mouse_x_pos, &old_mouse_y_pos);
    }
};

struct OrbitCamera
    : public Camera
{
    double old_x_pos = 0.;
    double old_y_pos = 0.;

    double phi = 0.;
    double theta = 0.;
    double r = 20.;

    void handleKey(GLFWwindow* window, int key, int action, int mods) override
    { }

    void update(GLFWwindow* window, double dt) override
    {
        Camera::update(window, dt);

        double speed = 1.;
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A)) {
            phi -= speed * dt;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D)) {
            phi += speed * dt;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W)) {
            theta += speed * dt;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S)) {
            theta -= speed * dt;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_R)) {
            r -= r * dt;
        }
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_F)) {
            r += r * dt;
        }
        theta = glm::clamp(theta, -glm::pi<double>() * .49, glm::pi<double>() * .49);
        view_matrix = createViewMatrix(phi, theta, r);
        updateFinalMatrix();
    }

    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override
    {
        r -= r * yoffset * .1;
        r = glm::clamp(r, .001, 1000.);
        view_matrix = createViewMatrix(phi, theta, r);
        updateFinalMatrix();
    }

    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override
    {
        if (GLFW_PRESS == glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
            double dx = xpos - old_x_pos, dy = ypos - old_y_pos;
            phi -= dx * .005;
            theta += dy * .005;
            theta = glm::clamp(theta, -glm::pi<double>() * .49, glm::pi<double>() * .49);
        }
        old_x_pos = xpos;
        old_y_pos = ypos;
    }

    static glm::mat4 createViewMatrix(double phi, double theta, double r)
    {
        glm::vec3 pos = glm::vec3(glm::cos(phi) * glm::cos(theta),
                                  glm::sin(phi) * glm::sin(theta),
                                  glm::sin(theta) + .5f) * static_cast<float>(r);
        return glm::lookAt(pos, glm::vec3(0.f, 0.f, 0.5f), glm::vec3(0.f, 0.f, 1.f));
    }

    explicit OrbitCamera(GLFWwindow* window)
        : Camera(createViewMatrix(0, 0, 20), window)
    {
    }
};

CameraPtr Camera::makeGodCamera(GLFWwindow* window)
{
    return std::make_shared<OrbitCamera>(window);
}

CameraPtr Camera::makePlayerCamera(GLFWwindow *window)
{
    return std::make_shared<PlayerCamera>(window);
}
