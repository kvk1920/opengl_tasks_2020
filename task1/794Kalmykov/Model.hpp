#pragma once

#include "OpenGL.hpp"
#include "Shader.hpp"

#include <memory>

class Model
{
    friend class Drawable;

    ShaderProgramPtr shader;

protected:
    explicit Model(ShaderProgramPtr shader);
    virtual void draw() = 0;

public:
    virtual ~Model() = default;

    static std::shared_ptr<Model> makeWall(ShaderProgramPool& shader_pool);
};

using ModelPtr = std::shared_ptr<Model>;
