#include "Resources.hpp"
#include "Shader.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

static std::string loadFile(std::string_view name)
{
    std::ifstream input(requireResourcePath(name));
    std::string src, line;
    while (std::getline(input, line)) {
        src.append(line);
        src.push_back('\n');
    }
    return src;
}

Shader::Shader(std::string name, GLenum shader_type)
{
    shader_id = glCreateShader(shader_type);
    name.append(GL_VERTEX_SHADER == shader_type ? ".vert" : ".frag");
    auto src = loadFile(name);
    const char* text = src.data();
    glShaderSource(shader_id, 1, &text, nullptr);
    glCompileShader(shader_id);
    int status = -1;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint error_length;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &error_length);
        std::vector<char> msg(error_length + 1);
        glGetShaderInfoLog(shader_id, error_length, nullptr, msg.data());
        std::cerr << msg.data();
        exit(1);
    }
}

Shader::~Shader()
{
    glDeleteShader(shader_id);
}

ShaderPtr ShaderLoader::loadVertexShader(const std::string& name)
{
    if (auto it = vertex_shaders.find(name); it != vertex_shaders.end()) {
        return it->second;
    }
    return vertex_shaders[name] = ShaderPtr(new Shader(name, GL_VERTEX_SHADER));
}

ShaderPtr ShaderLoader::loadFragmentShader(const std::string& name)
{
    if (auto it = fragment_shaders.find(name); it != fragment_shaders.end()) {
        return it->second;
    }
    return fragment_shaders[name] = ShaderPtr(new Shader(name, GL_FRAGMENT_SHADER));
}

ShaderProgram::ShaderProgram(ShaderPtr vertex_shader, ShaderPtr fragment_shader)
    : vertex_shader(std::move(vertex_shader))
    , fragment_shader(std::move(fragment_shader))
{
    program_id = glCreateProgram();
    glAttachShader(program_id, this->vertex_shader->shader_id);
    glAttachShader(program_id, this->fragment_shader->shader_id);
    glLinkProgram(program_id);
    int status = -1;
    glGetProgramiv(program_id, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        GLint errorLength;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &errorLength);
        std::vector<char> errorMessage;
        errorMessage.resize(errorLength);
        glGetProgramInfoLog(program_id, errorLength, nullptr, errorMessage.data());
        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;
        exit(1);
    }
}

ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(program_id);
}

ShaderProgramPtr ShaderProgram::make(ShaderPtr vertex_shader, ShaderPtr fragment_shader)
{
    return ShaderProgramPtr(new ShaderProgram(std::move(vertex_shader), std::move(fragment_shader)));
}

ShaderProgramPtr ShaderProgram::make(ShaderLoader &loader,
                                     const std::string& vertex_shader_name,
                                     const std::string& fragment_shader_name)
{
    return make(loader.loadVertexShader(vertex_shader_name),
                loader.loadFragmentShader(fragment_shader_name));
}

void ShaderProgram::use() const
{
    glUseProgram(program_id);
    for (const auto& [name, info] : mat4_uniforms) {
        glUniformMatrix4fv(info.first, 1, GL_FALSE, glm::value_ptr(info.second));
    }
    for (const auto& [name, info] : vec4_uniforms) {
        glUniform4fv(info.first, 1, glm::value_ptr(info.second));
    }
}

void ShaderProgram::requireUniformMat4(const std::string &name)
{
    if (auto it = mat4_uniforms.find(name); mat4_uniforms.end() == it) {
        GLint uniform_id = glGetUniformLocation(program_id, name.c_str());
        if (-1 == uniform_id) {
            std::cerr << "unknown uniform variable " << name << std::endl;
            exit(1);
        }
        mat4_uniforms.insert({name, {uniform_id, {}}});
    }
}

void ShaderProgram::requireUniformVec4(const std::string &name)
{
    if (auto it = vec4_uniforms.find(name); vec4_uniforms.end() == it) {
        GLint uniform_id = glGetUniformLocation(program_id, name.c_str());
        if (-1 == uniform_id) {
            std::cerr << "unknown uniform variable " << name << std::endl;
            exit(1);
        }
        vec4_uniforms.insert({name, {uniform_id, {}}});
    }
}

void ShaderProgram::setUniformMat4(const std::string& name, glm::mat4 value)
{
    mat4_uniforms.at(name).second = value;
}

void ShaderProgram::setUniformVec4(const std::string& name, glm::vec4 value)
{
    vec4_uniforms.at(name).second = value;
}

ShaderProgramPtr ShaderProgramPool::createProgram(std::string program_name, ShaderPtr vertex, ShaderPtr fragment)
{
    return pool.emplace(std::move(program_name),
                        ShaderProgram::make(std::move(vertex), std::move(fragment))
                        ).first->second;
}

ShaderProgramPtr ShaderProgramPool::createProgram(std::string program_name,
                                                  ShaderLoader& loader,
                                                  std::string vertex,
                                                  std::string fragment)
{
    return createProgram(std::move(program_name), loader.loadVertexShader(vertex), loader.loadFragmentShader(fragment));
}

ShaderProgramPtr ShaderProgramPool::getProgram(const std::string& program_name) const
{
    return pool.at(program_name);
}
