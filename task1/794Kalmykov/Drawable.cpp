#include "Drawable.hpp"

#include <glm/geometric.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/vector_angle.hpp>

Drawable::Drawable(glm::mat4 model_matrix, ModelPtr model)
    : model_matrix(model_matrix)
    , model(std::move(model))
{ }

void Drawable::draw(const Camera& camera)
{
    model->shader->use();
    model->shader->setUniformMat4("m_matrix", model_matrix);
    model->shader->setUniformMat4("vp_matrix", camera.vp_matrix);
    drawImpl(*(model->shader));
    model->draw();
}

struct Square
    : public Drawable
{
    static glm::mat4 createModelMatrix(glm::vec2 from, glm::vec2 to, WallType wall_type)
    {
        glm::mat4 model_matrix(1.f);
        model_matrix = glm::scale(model_matrix, glm::vec3(glm::length(to - from)));
        model_matrix = glm::translate(model_matrix, glm::vec3(from.x, from.y,
                                                              WallType::Ceil == wall_type ? 1.f : 0.f));
        model_matrix = glm::rotate(model_matrix,
                                   glm::orientedAngle(glm::vec2(1.f, 0.f), to - from),
                                   glm::vec3(0.f, 0.f, 1.f));
        if (wall_type != WallType::Wall) {
            model_matrix = glm::rotate(model_matrix,
                                       -glm::radians(90.f),
                                       glm::vec3(to - from, 0.f));
        }
        return model_matrix;
    }

    Square(ModelPtr square_model,
         glm::vec2 from,
         glm::vec2 to,
         WallType wall_type)
         : Drawable(createModelMatrix(from, to, wall_type), std::move(square_model))
    { }

    void drawImpl(ShaderProgram& shader) override
    { }
};

DrawablePtr Drawable::makeSquare(ModelPtr wall_model,
                                 glm::vec2 from,
                                 glm::vec2 to,
                                 WallType wall_type)
{
    return std::make_shared<Square>(std::move(wall_model), from, to, wall_type);
}
