#include "Model.hpp"

#include "OpenGL.hpp"

Model::Model(ShaderProgramPtr shader)
    : shader(std::move(shader))
{ }

struct WallModel
    : public Model
{
    GLuint vertex_buffer{};
    GLuint index_buffer{};
    GLuint vertex_array{};

    struct Vertex
    {
        glm::vec3 position;
        glm::vec3 normal;
    };

    static constexpr Vertex points[4] = {
        {
            glm::vec3(0.f, 0.f, 0.f),
            glm::vec3(0.f, -1.f, 0.f),
        },
        {
            glm::vec3(1.f, 0.f, 0.f),
            glm::vec3(0.f, -1.f, 0.f),
        },
        {
            glm::vec3(1.f, 0.f, 1.f),
            glm::vec3(0.f, -1.f, 0.f),
        },
        {
            glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(0.f, -1.f, 0.f),
        },
    };

    static constexpr unsigned short indices[2][3] = {
        {0, 1, 2},
        {0, 2, 3},
    };

    explicit WallModel(ShaderProgramPool& shader_pool)
        : Model(shader_pool.getProgram("wall"))
    {
        glGenBuffers(1, &vertex_buffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
        glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(Vertex), points, GL_STATIC_DRAW);

        glGenBuffers(1, &index_buffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned short), indices, GL_STATIC_DRAW);

        glGenVertexArrays(1, &vertex_array);
        glBindVertexArray(vertex_array);
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(0));
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(sizeof(glm::vec3)));
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);
        glBindVertexArray(0);
    }

    void draw() override
    {
        glBindVertexArray(vertex_array);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<void*>(0));
        glBindVertexArray(0);
    }
};

ModelPtr Model::makeWall(ShaderProgramPool &shader_pool)
{
    return std::make_shared<WallModel>(shader_pool);
}
