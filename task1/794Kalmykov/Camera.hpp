#pragma once

#include "OpenGL.hpp"

#include <memory>

#include "Interactive.hpp"

class Camera
    : public Interactive
{
    friend class Drawable;

    glm::mat4 projection_matrix;
    glm::mat4 vp_matrix{};

    static constexpr double near_plane = .1f, far_plane = 100.f;

    static glm::mat4 createProjectionMatrix(GLFWwindow* window);

protected:
    glm::mat4 view_matrix;

    Camera(glm::mat4 view_matrix, GLFWwindow* window);
    void updateFinalMatrix();

public:
    static std::shared_ptr<Camera> makeGodCamera(GLFWwindow* window);
    static std::shared_ptr<Camera> makePlayerCamera(GLFWwindow* window);

    void update(GLFWwindow* window, double delta_time) override;
};

using CameraPtr = std::shared_ptr<Camera>;
