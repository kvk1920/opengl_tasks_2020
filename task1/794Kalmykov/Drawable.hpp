#pragma once

#include "Camera.hpp"
#include "Model.hpp"
#include "OpenGL.hpp"
#include "Shader.hpp"

enum class WallType
{
    Floor,
    Ceil,
    Wall,
};

class Drawable
{
    glm::mat4 model_matrix;
    ModelPtr model;

protected:
    explicit Drawable(glm::mat4 model_matrix, ModelPtr model);
    virtual void drawImpl(ShaderProgram& shader) = 0;

public:
    virtual ~Drawable() = default;
    virtual void draw(const Camera& camera);

    static std::shared_ptr<Drawable> makeSquare(
        ModelPtr wall_model,
        glm::vec2 from,
        glm::vec2 to,
        WallType wall_type);
};

using DrawablePtr = std::shared_ptr<Drawable>;
