#version 330

in vec3 normal;

out vec4 fragment_color;

void main()
{
    if (gl_FrontFacing) {
        fragment_color = vec4(normal * 0.5 + 0.5, 1);
    } else {
        fragment_color = vec4(-normal * 0.5 + 0.5, 1);
    }
}
