#version 330

layout(location = 0) in vec3 vertex_position;
layout(location = 1) in vec3 vertex_normal;

uniform mat4 m_matrix;
uniform mat4 vp_matrix;

out vec3 normal;

void main()
{
    gl_Position =  vp_matrix * m_matrix * vec4(vertex_position, 1.0);
    vec4 normal4 = m_matrix * vec4(vertex_normal, vec4(vertex_normal, 0.0));
    normal = vec3(normal4.x, normal4.y, normal4.z);
}
