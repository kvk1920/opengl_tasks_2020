#include "Application.hpp"

#include <iostream>

Application* Application::getApplication(GLFWwindow *window)
{
    return reinterpret_cast<Application*>(glfwGetWindowUserPointer(window));
}

void Application::handleKey(GLFWwindow* window, int key, int action, int mods)
{
    if (GLFW_PRESS == action && GLFW_KEY_ESCAPE == key) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
    if (GLFW_PRESS == action && GLFW_KEY_SPACE == key) {
        if (active_camera == player_camera) {
            active_camera = god_camera;
        } else {
            active_camera = player_camera;
        }
    }
    active_camera->handleKey(window, key, action, mods);
}

void Application::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    getApplication(window)->handleKey(window, key, action, mods);
}

void Application::mouseCursorPosCallback(GLFWwindow *window, double xpos, double ypos)
{
    getApplication(window)->handleMouseMove(window, xpos, ypos);
}

void Application::scrollCallback(GLFWwindow* window, double xpos, double ypos)
{
    getApplication(window)->handleScroll(window, xpos, ypos);
}

void Application::handleMouseMove(GLFWwindow *window, double xpos, double ypos)
{
    active_camera->handleMouseMove(window, xpos, ypos);
}

void Application::handleScroll(GLFWwindow *window, double xoffset, double yoffset)
{
    active_camera->handleScroll(window, xoffset, yoffset);
}

void Application::initContext()
{
    if (!glfwInit())
    {
        std::cerr << "ERROR: could not start GLFW3\n";
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

    int monitors_count;
    GLFWmonitor** monitors = glfwGetMonitors(&monitors_count);

    GLFWmonitor* monitor = monitors[monitors_count - 1];
    const GLFWvidmode* video_mode = glfwGetVideoMode(monitor);
    std::cout << "monitor width = " << video_mode->width << std::endl;
    std::cout << "monitor height = " << video_mode->height << std::endl;
    window = glfwCreateWindow(video_mode->width, video_mode->height, "Labirint", nullptr, nullptr);

    if (!window)
    {
        std::cerr << "ERROR: could not open window with GLFW3\n";
        glfwTerminate();
        exit(1);
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetCursorPosCallback(window, mouseCursorPosCallback);
    glfwSetScrollCallback(window, scrollCallback);
}

void Application::initGL()
{
    glewExperimental = GL_TRUE;
    glewInit();

    const GLubyte* renderer = glGetString(GL_RENDERER); //Получаем имя рендерера
    const GLubyte* version = glGetString(GL_VERSION); //Получаем номер версии
    const GLubyte* glslversion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    std::cout << "Renderer: " << renderer << std::endl;
    std::cout << "OpenGL context version: " << version << std::endl;
    std::cout << "GLSL version: " << glslversion << std::endl;

    glGetError();


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void Application::initCamera()
{
    god_camera = Camera::makeGodCamera(window);
    player_camera = Camera::makePlayerCamera(window);
    active_camera = god_camera;
}

Application::~Application()
{
    glfwTerminate();
}

void Application::start()
{
    initContext();
    initGL();
    initCamera();
    initShaders();
    initModels();
    makeScene();
    run();
}

void Application::run()
{
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        update();
        {
            int width, height;
            glfwGetFramebufferSize(window, &width, &height);
            glViewport(0, 0, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }
        draw();
        glfwSwapBuffers(window);
    }
}

void Application::initShaders()
{
    auto wall_shader = program_pool.createProgram("wall", shader_loader, "wall", "wall");
    wall_shader->requireUniformMat4("m_matrix");
    wall_shader->requireUniformMat4("vp_matrix");
}

void Application::initModels()
{
    wall_model = Model::makeWall(program_pool);
}

void Application::makeScene()
{
    std::cout << "makeScene() started" << std::endl;
    map.setModel(wall_model);
    map.load("level1");
    std::cout << "makeScene() finished" << std::endl;
}

void Application::update()
{
    auto new_time = glfwGetTime();
    auto dt = new_time - old_time;
    old_time = new_time;

    update(window, dt);
}

void Application::update(GLFWwindow* window, double delta_time)
{
    active_camera->update(window, delta_time);
}

void Application::draw()
{
    map.draw(*active_camera);
}
