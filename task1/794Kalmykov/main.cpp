#include <iostream>

#include "Application.hpp"

int main()
{
    Application app;
    app.start();
    std::cout << "Hello, World!" << std::endl;
}