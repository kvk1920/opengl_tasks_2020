#include "Resources.hpp"

#include <iostream>

std::filesystem::path requireResourcePath(std::string_view resource_name)
{
    std::filesystem::path path("794KalmykovData1");
    path /= resource_name;
    if (std::filesystem::exists(path)) {
        std::cout << path << " found" << std::endl;
    } else {
        std::cerr << path << "not found" << std::endl;
        exit(1);
    }
    return path;
}
