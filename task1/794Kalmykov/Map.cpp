#include "Map.hpp"
#include "Resources.hpp"

#include <fstream>
#include <iostream>

namespace fs = std::filesystem;

static std::vector<std::string> loadMapFile(std::string str)
{
    str.append(".map");
    std::ifstream input(requireResourcePath(str));
    std::vector<std::string> map;
    std::string line;
    while (std::getline(input, line)) {
        map.emplace_back(std::move(line));
        line.clear();
    }
    constexpr static auto report_error = []
    {
        std::cerr << "invalid map config" << std::endl;
        exit(1);
    };
    if (map.empty()) {
        report_error();
    }
    for (const auto& row : map) {
        if (row.length() != map.front().length()) {
            report_error();
        }
    }
    return map;
}

static void processCell(const std::vector<std::string>& map,
                        std::vector<DrawablePtr>& objects,
                        const ModelPtr& model,
                        int i, int j)
{
    auto n = static_cast<int>(map.size());
    auto m = static_cast<int>(map.front().size());
    //std::cout << "map[" << i << "][" << j << "] = " << map[i][j] << std::endl;
    if ('#' == map[i][j]) {
        return;
    }
    //std::cout << "added some objects" << std::endl;
    objects.emplace_back(Drawable::makeSquare(model,
                                              {i, j},
                                              {i + 1, j},
                                              WallType::Floor));

    objects.emplace_back(Drawable::makeSquare(model,
                                              {i, j},
                                              {i + 1, j},
                                              WallType::Ceil));
    if (i + 1 < n && '#' == map[i + 1][j]) {
        objects.emplace_back(Drawable::makeSquare(
            model,
            {i + 1, j},
            {i + 1, j + 1},
            WallType::Wall
            ));
    }
    if (i > 0 && '#' == map[i - 1][j]) {
        objects.emplace_back(Drawable::makeSquare(
            model,
            {i, j + 1},
            {i, j},
            WallType::Wall
            ));
    }
    if (j + 1 < m && '#' == map[i][j + 1]) {
        objects.emplace_back(Drawable::makeSquare(
            model,
            {i + 1, j + 1},
            {i, j + 1},
            WallType::Wall
            ));
    }
    if (j > 0 && '#' == map[i][j - 1]) {
        objects.emplace_back(Drawable::makeSquare(
            model,
            {i, j},
            {i + 1, j},
            WallType::Wall
            ));
    }
    std::cout << "current object.size is " << objects.size() << std::endl;
}

void Map::draw(const Camera& camera)
{
    for (const auto& object : objects) {
        object->draw(camera);
    }
}

void Map::setModel(ModelPtr wall_model1)
{
    this->wall_model = std::move(wall_model1);
}

void Map::load(std::string str)
{
    objects.clear();
    const auto map = loadMapFile(std::move(str));
    std::cout << "map width is " << map.size() << std::endl;
    for (size_t i = 0; i < map.size(); ++i) {
        for (size_t j = 0; j < map[0].length(); ++j) {
            processCell(map, objects, wall_model,
                        static_cast<int>(i),
                        static_cast<int>(j));
        }
    }
    std::cout << "created map with " << objects.size() << " objects" << std::endl;
}
